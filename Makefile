MAKEFILE	=Makefile
CC     		=gcc
INCLUDE		=-I/usr/include/xulrunner-1.9/stable -I/usr/include/nspr
CFLAGS 		=-Wall -fPIC
LIBS		=-lGL -lGLU -lm
LIBSPATH	=-L/usr/lib/nspr -Wl,-R/usr/lib/nspr

OBJS		=ns-unix.o core.o common.o action.o
OUTPUT		=libglugin.so

Q 		:= @

.PHONY: build
.PHONY: clean
.PHONY: install

build: ${OUTPUT}

clean:
	$(Q)rm -f $(OBJS) $(OUTPUT)

install:
	$(Q)mkdir -p ~/.mozilla/plugins/
	$(Q)cp libglugin.so ~/.mozilla/plugins/libglugin.so -v
	@printf "  CP  ${OUTPUT}\n"

pack:
	zip firefox-linux-x86-64.zip $(OUTPUT)

.c.o:
	@printf "  CC  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(CC) $(CFLAGS) $(INCLUDE) -c -o$@ $<

ns-unix.o:	ns-unix.c	$(MAKEFILE)
core.o:		core.c		$(MAKEFILE)
common.o:	common.c	$(MAKEFILE)
action.o:	action.c	$(MAKEFILE)

${OUTPUT}: $(OBJS) $(MAKEFILE)
	@printf "  LD  $(subst $(shell pwd)/,,$(@))\n";
	$(Q)$(CC) $(LIBSPATH) $(LIBS) -shared -o$@ $(OBJS)
