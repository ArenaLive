/*
 *  (C) Copyright 2009 ZeXx86 (zexx86@gmail.com)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Based on GLugin http://exa.czweb.org/repos/glugin.tar.bz2
 *   Big thanks for "exa"
 */


#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "glugin.h"


int plugin_action_0 (pluginData *pd)
{
	Log ("action_0");

	return creat ("/tmp/.arenalivep", 0777);
}

int plugin_action_1 (pluginData *pd)
{
	int s, s2, t, l;
        struct sockaddr_un local, remote;

        if ((s = socket (AF_UNIX, SOCK_STREAM, 0)) == -1) {
		Log ("socket == -1");
		return -1;
        }

        local.sun_family = AF_UNIX;
        strcpy (local.sun_path, "/tmp/.arenalive");
        unlink (local.sun_path);

        l = strlen (local.sun_path) + sizeof (local.sun_family);

        if (bind (s, (struct sockaddr *) &local, l) == -1) {
		Log ("bind == -1");
		return -1;
	}

        if (listen (s, 5) == -1)
		return -1;

	t = sizeof (remote);

	pid_t child = fork ();

	if (child == -1)
		return -1;

	if (!child) {
		char *user = getenv ("USER");

		if (!user)
			return -1;

		if (strlen (user) > 32) {
			Log ("ERROR -> env USER - too long");
			_exit (0);
		}

		unsigned l = strlen (pd->conparam);
		if (l > 256) {
			Log ("ERROR -> pd->conparam - too long");
			_exit (0);
		}

		conparam_verify (pd->conparam, l);

		/* prevent before multiple instances */
		system ("killall arenalive-linux -s9");

		char cmd[512];

		// ~80 + 3*32 + 128 + 15 < 512 chars
		snprintf (cmd, 511, "LD_LIBRARY_PATH=/home/%s/.arenalive/ /home/%s/.arenalive/%s %s &> /home/%s/.arenalive/log",
			user, user, "arenalive-linux", pd->conparam, user);

		Log ("-- %s", cmd);

		system (cmd);

		_exit (0);
	}

	if ((s2 = accept (s, (struct sockaddr *) &remote, (unsigned *) &t)) == -1) {
		Log ("accept == -1");
		return -1;
	}
  
	char str[sizeof (Window)+1];
    
	memcpy (str, &pd->win, sizeof (Window));

        if (send (s2, str, sizeof (Window), 0) < 0)
		return -1;

        close (s2);

	/* plugin loop */
	glugin_proc (pd);

	return 0;
}

int plugin_action (pluginData *pd)
{
	switch (pd->action) {
		case 0:
			return plugin_action_0 (pd);
		case 1:
			return plugin_action_1 (pd);
	}

	return -1;
}

