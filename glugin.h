/*
 *  (C) Copyright 2009 ZeXx86 (zexx86@gmail.com)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Based on GLugin http://exa.czweb.org/repos/glugin.tar.bz2
 *   Big thanks for "exa"
 */


#define MOZ_X11
#include "npapi.h"
#include "npupp.h"

void Log (const char *, ...);

#define pdof(x) ((pluginData *) (x->pdata))

typedef struct pluginData_t {
	/* UNIX part */
	Window win;
	pid_t child;
	FILE *pr, *pw;
	Display *dpy;

	int has_window;

	/* common part */
	void (*swapbuffers) (struct pluginData_t *);
	void (*getsize) (struct pluginData_t *, int *, int *); /* TODO */

	int exit_request;
	int exit_ack;

	/* html tag parameters */
	char *conparam;
	int action;

	/* guest handlers */
	void (*handle_newstream) (struct pluginData_t *, NPStream *);
	void (*handle_destroystream) (struct pluginData_t *, NPStream *);
	void (*handle_write) (struct pluginData_t *, NPStream *, int32, int32, void *);
} pluginData;

void glugin_proc (pluginData *pd);


//op number, unsigned int
#define gln_new_stream 		1 /* stream * */
#define gln_stream_data 	2 /* stream *, data offset, data size, data */
#define gln_destroy_stream 	3 /* stream * */
#define gln_request_get 	4 /* url size, url */
#define gln_request_post 	5 /* url size, data size, url, data */

void host_newstream (pluginData *, NPStream *);
void host_destroystream (pluginData *, NPStream *);
void host_write (pluginData *, NPStream *, int32, int32, void *);
void host_read_guest_requests (pluginData *);
void guest_read_host_requests (pluginData *);

extern int plugin_action (pluginData *pd);
extern void conparam_verify (char *str, unsigned len);
