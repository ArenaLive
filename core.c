/*
 *  (C) Copyright 2009 ZeXx86 (zexx86@gmail.com)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Based on GLugin http://exa.czweb.org/repos/glugin.tar.bz2
 *   Big thanks for "exa"
 */


#include "glugin.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <unistd.h>
#include <math.h>

void guest_read_host_requests (pluginData *pd)
{
	int cmd;
	NPStream *s;

	if (fread (&cmd, sizeof (int), 1, pd->pr) <= 0)
		return; //no data

	switch (cmd) {
		case gln_new_stream:
			fread (&s, sizeof (s), 1, pd->pr);

			pd->handle_newstream (pd,s);
			break;
		case gln_destroy_stream:
			fread (&s, sizeof (s), 1, pd->pr);

			pd->handle_destroystream (pd, s);
			break;
		case gln_stream_data:
			{
				int32 off, len;
				void *data;

				fread (&s, sizeof (s), 1, pd->pr);
				fread (&off, sizeof (off), 1, pd->pr);
				fread (&len, sizeof (len), 1, pd->pr);

				if (len > 0) {
					data = malloc (len);

					fread (data,len,1,pd->pr);

					pd->handle_write (pd, s, off, len, data);

					free (data);
				} else
					Log ("w: zero len!");
			}
			break;
		default:
			Log ("Unhandled request to guest: %d", cmd);
	}
}

void guest_newstream (pluginData *pd, NPStream *s)
{
	Log ("guest has NEWSTREAM: %p",s);
}

void guest_destroystream (pluginData *pd, NPStream *s)
{
	Log ("guest has DESTROYSTREAM: %p",s);
}

void guest_write (pluginData *pd, NPStream*s, int32 off, int32 len, void *data)
{
	Log ("guest has WRITE: %p %d %d, data followz:", s, off, len);

	int i;
	for (i = 0; i < len; ++ i)
		Log ("%8X: %c",i, ((char *) data)[i]);
}

void glugin_proc (pluginData *pd)
{
	while (!(pd->exit_request)) {
		guest_read_host_requests (pd);

		usleep (1000);
	}

	pd->exit_ack = 1;
}
