/*
 *  (C) Copyright 2009 ZeXx86 (zexx86@gmail.com)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Based on GLugin http://exa.czweb.org/repos/glugin.tar.bz2
 *   Big thanks for "exa"
 */


#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>

void Log (const char *c, ...)
{
#ifdef ENABLE_LOG
	va_list al;

	FILE *f = fopen ("~/glugin.log", "a");

	if (!f)
		return;

	va_start (al, c);
	vfprintf (f, c, al);
	va_end (al);

	fputc ('\n', f);
	fclose (f);
#endif
}

/* replace unallowed characters with space */
void conparam_verify (char *str, unsigned len)
{
	unsigned i;
	for (i = 0; i < len; i ++) {
		if (str[i] == '$' || str[i] == '(' || str[i] == ')' || str[i] == ';' || str[i] == '|' || str[i] == '<' || str[i] == '>')
			str[i] = ' ';
	}
}