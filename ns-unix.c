/*
 *  (C) Copyright 2009 ZeXx86 (zexx86@gmail.com)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *   Based on GLugin http://exa.czweb.org/repos/glugin.tar.bz2
 *   Big thanks for "exa"
 */


#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <signal.h>

#include <GL/glx.h>

#include "glugin.h"

static NPNetscapeFuncs gNetscapeFuncs;	/* Netscape Function table */

/* prototypes */
void plugin_process_handler (pluginData *, Window, Visual *);


NPError NP_Initialize (NPNetscapeFuncs *nsTable, NPPluginFuncs *pluginFuncs)
{
	Log ("init");

	/* this is respectfully taken from mplayerplug-in, as it was observed
	 * and proved to be extremely useful. */
	if ((nsTable == NULL) || (pluginFuncs == NULL))
		return NPERR_INVALID_FUNCTABLE_ERROR;

	Log ("table ok");

	if ((nsTable->version >> 8) > NP_VERSION_MAJOR)
		return NPERR_INCOMPATIBLE_VERSION_ERROR;

	Log ("table version ok");

	/*check for table size removed, probable alignment or such problems.
	 *

	if (nsTable->size < sizeof(NPNetscapeFuncs))
		return NPERR_INVALID_FUNCTABLE_ERROR;
	Log("table size ok");

	 */

	if (pluginFuncs->size < sizeof (NPPluginFuncs))
		return NPERR_INVALID_FUNCTABLE_ERROR;

	Log ("ok pluginfuncs size check");

	Log ("ok initing");

	gNetscapeFuncs.version = nsTable->version;
	gNetscapeFuncs.size = nsTable->size;
	gNetscapeFuncs.posturl = nsTable->posturl;
	gNetscapeFuncs.geturl = nsTable->geturl;
	gNetscapeFuncs.requestread = nsTable->requestread;
	gNetscapeFuncs.newstream = nsTable->newstream;
	gNetscapeFuncs.write = nsTable->write;
	gNetscapeFuncs.destroystream = nsTable->destroystream;
	gNetscapeFuncs.status = nsTable->status;
	gNetscapeFuncs.uagent = nsTable->uagent;
	gNetscapeFuncs.memalloc = nsTable->memalloc;
	gNetscapeFuncs.memfree = nsTable->memfree;
	gNetscapeFuncs.memflush = nsTable->memflush;
	gNetscapeFuncs.reloadplugins = nsTable->reloadplugins;
	gNetscapeFuncs.getJavaEnv = nsTable->getJavaEnv;
	gNetscapeFuncs.getJavaPeer = nsTable->getJavaPeer;
	gNetscapeFuncs.getvalue = nsTable->getvalue;
	gNetscapeFuncs.setvalue = nsTable->setvalue;

	pluginFuncs->version = (NP_VERSION_MAJOR << 8) | NP_VERSION_MINOR;
	pluginFuncs->size = sizeof (NPPluginFuncs);
	pluginFuncs->newp = NPP_New;
	pluginFuncs->destroy = NPP_Destroy;
	pluginFuncs->event = NPP_HandleEvent;
	pluginFuncs->setwindow = NPP_SetWindow;
	pluginFuncs->print = NPP_Print;
	pluginFuncs->newstream = NPP_NewStream;
	pluginFuncs->destroystream = NPP_DestroyStream;
	pluginFuncs->asfile = NPP_StreamAsFile;
	pluginFuncs->writeready = NPP_WriteReady;
	pluginFuncs->write = NPP_Write;

	return NPERR_NO_ERROR;
}

NPError NP_Shutdown ()
{
	Log ("shutdown");

	/* prevent before multiple instances */
	system ("killall arenalive-linux -s9");

	return NPERR_NO_ERROR;
}


char *NP_GetMIMEDescription ()
{
	Log ("get mime");
	return "x-application/glugin:gln:Glugin;";
}

NPError NP_GetValue (void *instance, NPPVariable variable, void *value)
{
	Log ("get value");

	switch (variable) {
		case NPPVpluginNameString:
			(*(char **) value) = "Arena Live";
			break;
		case NPPVpluginDescriptionString:
			(*(char **) value) = "Arena Live plugin";
			break;
		default:
			return NPERR_INVALID_PARAM;
	}

	return NPERR_NO_ERROR;
}

NPError NPP_New (
	NPMIMEType mime,
	NPP instance,
	uint16 mode,
	int16 argc, char *argn[], char *argv[],
	NPSavedData *saved)
{
	int i;
	pluginData *pd;

	Log ("new p");

	gNetscapeFuncs.setvalue (instance, NPPVpluginTransparentBool, false);

	instance->pdata = gNetscapeFuncs.memalloc (sizeof (pluginData));

	Log ("allocated");
	
	pd = pdof (instance);

	pd->exit_request = 0;
	pd->exit_ack = 0;
	pd->has_window = 0;

	pd->conparam = 0;
	pd->action = -1;

	for (i = 0; i < argc; ++ i) { 
		if (!strcmp (argn[i], "conparam")) {
			Log ("html tag parameter - conparam");

			pd->conparam = strdup (argv[i]);
		} else
		if (!strcmp (argn[i], "action")) {
			Log ("html tag parameter - action");

			pd->action = atoi (argv[i]);
		}

	}

	return NPERR_NO_ERROR;
}

void set_nonblock (int fd)
{
	long flags = fcntl (fd, F_GETFL, 0);

	flags |= (O_NONBLOCK | O_NDELAY);

	fcntl (fd, F_SETFL, flags);
}

NPError NPP_SetWindow (NPP instance, NPWindow *w)
{
	pluginData *pd = pdof (instance);
	pid_t child;
	int pw[2], pr[2];

	if (!w) {
		Log ("setwindow: destroyed");
		return NPERR_NO_ERROR;
	}

	if (!pd->has_window) {
		pd->has_window = 1;

		pipe (pw);
		pipe (pr);

		set_nonblock (pr[0]);
		set_nonblock (pr[1]);
		set_nonblock (pw[0]);
		set_nonblock (pw[1]);

		child = fork ();

		if (child == -1)
			return NPERR_MODULE_LOAD_FAILED_ERROR;

		if (!child) {
			Log ("in new process!");

			close (pw[1]);
			close (pr[0]);

			pd->pw = fdopen (pr[1], "w");
			pd->pr = fdopen (pw[0], "r");

			/* TODO check if setsid is needed here */

			plugin_process_handler (pd, pd->win = (Window) (w->window), ((NPSetWindowCallbackStruct *) (w->ws_info))->visual);

			_exit (0);
		} else {
			pd->child = child;

			close (pw[0]);
			close (pr[1]);

			pd->pw = fdopen (pw[1], "w");
			pd->pr = fdopen (pr[0], "r");
		}
	}
	
	// we don't really care about window resize events, as long as
	// it can catch them by native methods

	return NPERR_NO_ERROR;
}


int16 NPP_HandleEvent (NPP instance, void *event)
{
	Log ("event");

	return true;
}

NPError NPP_Destroy (NPP instance, NPSavedData **saved)
{
	pluginData *pd = pdof (instance);

	Log ("killing child...");

	kill (pd->child, SIGUSR1);

	fclose (pd->pw);
	fclose (pd->pr);

	usleep (10000);

	//TODO
	//wait until the child really terminates. Mutexes?

	gNetscapeFuncs.memfree (instance->pdata);
	Log ("deleted ok");

	return NPERR_NO_ERROR;
}

void NPP_Print (NPP instance, NPPrint *printInfo)
{
	Log ("attempt to print");
}

NPError NPP_NewStream (NPP instance, NPMIMEType type, NPStream *stream,	NPBool seekable, uint16* stype)
{
	Log ("new stream");

	host_newstream (pdof (instance), stream);

	return NPERR_NO_ERROR;
}

NPError NPP_DestroyStream (NPP instance, NPStream *stream, NPReason reason)
{
	Log ("destroy stream");

	host_destroystream (pdof (instance), stream);

	return NPERR_NO_ERROR;
}

int32 NPP_Write (NPP instance, NPStream *stream, int32 offset, int32 len, void *buf)
{
	Log ("write %d",len);

	host_write (pdof (instance), stream, offset, len, buf);

	return len;
}

void NPP_StreamAsFile (NPP instance, NPStream* stream, const char* fname)
{
	Log ("as file:");
	Log (fname);
}

int32 NPP_WriteReady (NPP instance, NPStream *stream)
{
	Log("write ready");

	return 1024; //ooh replace.
}

void resize (Display *dpy, Window win)
{
	XWindowAttributes wa;

	XGetWindowAttributes (dpy, win, &wa);

	glViewport (0,0,wa.width, wa.height);
}


static pluginData *g_pd;

void sig_usr1 (int i)
{
	g_pd->exit_request = 1;
}

void swapbuffers (pluginData *pd)
{
	glXSwapBuffers (pd->dpy, pd->win);
}

void plugin_process_handler (pluginData *pd, Window win, Visual *vis)
{
	g_pd = pd;

	pd->win = win;

	signal (SIGUSR1, sig_usr1);

	/* call action function */
	plugin_action (pd);

	Log ("plugin killed, cleaning up.");

	fclose (pd->pw);
	fclose (pd->pr);
	
	//glXMakeCurrent(dpy, None, 0);
	//glXDestroyContext(dpy,ctx);

	if (pd->conparam)
		free (pd->conparam);
}

void writedata (FILE *f, void *x, unsigned len)
{
	fwrite (x, len, sizeof (char), f);
}

void host_newstream (pluginData *pd, NPStream *s)
{
	writedata (pd->pw, (void *) gln_new_stream, sizeof (int));
	writedata (pd->pw, (void *) &s, sizeof (NPStream));

	fflush (pd->pw);
}

void host_destroystream (pluginData*pd, NPStream*s)
{
	writedata (pd->pw, (void *) gln_destroy_stream, sizeof (int));
	writedata (pd->pw, (void *) s, sizeof (NPStream));

	fflush (pd->pw);
}

void host_write (pluginData *pd, NPStream *s, int32 off, int32 siz, void *data)
{
	writedata (pd->pw, (void *) gln_stream_data, sizeof (int));
	writedata (pd->pw, (void *) s, sizeof (NPStream));
	writedata (pd->pw, (void *) &off, sizeof (off));
	writedata (pd->pw, (void *) &siz, sizeof (off));

	fwrite (data, siz, sizeof (char), pd->pw);

	fflush (pd->pw);
}

void host_read_guest_requests (pluginData *pd)
{
	Log ("Reading guest requests");
}
